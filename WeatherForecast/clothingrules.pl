:- module(clothingrules, [get_day_items/2
                         ]
                     ).

/*
Module clothingrules

Contains the rules that determine if an item is recommended
for the weather forecasts as well as functors to assemble
a set of items for recommendation, based on the day forecasts.
*/

%! item(-Item, Forecast) is det.
%  Rules to determine if an item should be recommended
% Condition attributes include [cloudiness, temperature, humidity, rain, wind, description, dt, hour, date].
item("a jumper", Conditions) :- Conditions.temperature < 15.
item("a coat", Conditions) :- Conditions.temperature < 10 ; Conditions.temperature < 15, Conditions.rain > 7.6.
item("an umbrella", Conditions) :- Conditions.rain > 2.5, Conditions.wind < 30.
item("a sunhat", Conditions) :- Conditions.wind < 40, Conditions.cloudiness < 10, Conditions.temperature > 20.
item("a warm hat", Conditions) :- Conditions.temperature < 3.
item("some gloves", Conditions) :- Conditions.temperature < 3.
item("a scarf", Conditions) :- Conditions.temperature < 3 ; Conditions.temperature < 5 , Conditions.wind > 10.
item("some sunglasses", Conditions) :- Conditions.cloudiness < 5.

%! items_for_forecast(+Forecast, -Items) is det.
%  Get a list of all the items recommended for that weather forecast
items_for_forecast(Forecast, Items) :-
    findall(Item, item(Item, Forecast), Items).

%! get_day_items(+Forecasts, -Items) is det.
%  Get a set of all items recommended for that day's forecasts
get_day_items(Forecasts, Items) :-
    get_day_items_(Forecasts, I),
    flatten(I, Flat),
    list_to_set(Flat, Items).
get_day_items_([], []).
get_day_items_([H|T], [I, TI]) :-
    items_for_forecast(H, I),
    get_day_items_(T, TI).
