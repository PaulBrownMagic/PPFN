# Weather Clothing Recommendation System

You can use this to not only get a weather forecast for any city in
the world, but it will also recommend attire and accessories for you!
So if it's expected to be a little chilly, your weather forecast
will let you know you should wear a jumper. AI weather forecast!

However, this project is a little more involved, being split over multiple
files. It's a little too large to convey how it all works in the
README, but we'll give an overview. If you're new to Prolog, you
should be able to happily change the rules that recommend attire
and accessories. If you're more experienced, you should be able to hack
at this with pleasure. Let's break down the files first.

## Tour of the files

**Don't try to open `city_ids's.pl` until you've read this!**

So `city_ids.pl` is a huge file. It's a database of cities,
countries and ids for use with the open weather API. You can use this
to search for a particular city you want a forecast for if you're
not sure of the two-letter country-code:

```bash
?- id("Tartu", City, ID).
City = "EE",
ID = "588335".
```

**There's a file missing!**

You need an additional file called `secret_key.pl`, this file is not
included as it should be used to store your secret API key from
[openweathermap.org](https://openweathermap.org/). You'll need an
account, but it is free!

The contents of `secret_key.pl` follow this template, except you'll
need to insert your own secret key:

```prolog
:- module(secret_key, [api_key/1]).

%! api_key(-Key) is det.
%  This is my secret key, it's not going on GitLab!
api_key("<insert your secret key here>").
```

The rest of the files are:

- **`date_time.pl`**: contains utilities to make handling dates and
  times easier
- **`weatherdata.pl`**: get the data from open weather and
  manipulate it
- **`clothing_rules.pl`**: rules for choosing items to wear and
  retrieving them, this is the AI bit.
- **`weatherforecast.pl`**: This is the entry point, the file you'd
  load into `swipl`. It contains all the writing predicates.

## How To Query It

Load `weatherforecast.pl` with `swipl`:

```bash
?- forecast("London", "GB").
```

## Things To Play With

The first port of call for adaptations are in the rules in
`clothingrules.pl`, you can adapt them to suit your needs.

From there, you can dive through the predicates, they're
commented to describe what they're doing. Look for what is imported
from modules, and what calls what. There's quite a bit of
structure manipulation going on, which can be tricky to follow.
