:- module(weatherforecast, [forecast/2]).

:- use_module(weatherdata, [get_daily_data/3
                            ]
             ).
:- use_module(clothingrules, [get_day_items/2
                             ]
             ).
:- use_module(date_time, [date_day/2,
                          day_time_forecasts/2]).

/*
Module weatherforecast

Get a weather forecast from openweathermap.org,
present the information, and intelligently
recommend attire and accessories for the day.
*/


%! forecast(-City, -Country) is det.
%  write out the forecast for City, Country
%  Eg.  forecast("London", "GB").
%  valid for cities and country values in city_ids.pl
forecast(City, Country) :-
    get_daily_data(City, Country, Data),
    write_daily_forecasts(Data).

%! write_daily_forecasts(+Forecasts) is det.
%  write a day worth of forecasts, uses
%  choicepoint to write the next day, or not.
write_daily_forecasts([H|T]) :-
    H = [F|_],
    date(Year, Month, D) = F.date,
    date_day(date(Year, Month, D), Day),
    format("~nForecast for ~w ~d/~d/~d~n", [Day, D, Month, Year]),
    day_time_forecasts(H, Daytime),
    get_day_items(Daytime, Items),
    write_items(Items),
    nl,
    write_forecasts(H),
    writeln("Next day?") ;
    write_daily_forecasts(T).
write_daily_forecasts([]) :- writeln("Forecast complete").

%! write_items(+Items) is det.
%  write out the recommended items to stdout
write_items([]):- !.
write_items([Item|[]]) :-
    format("You might want ~w.~n", Item), !.
write_items(Items) :-
    write("You might need "),
    write_items_list(Items).
write_items_list([H, L|[]]) :-
    format("~w, and ~w.~n", [H, L]), !.
write_items_list([H|T]) :-
    format("~w, ", H),
    write_items_list(T).


%! write_forecasts(+Forecasts) is det.
%  write all forecasts to stdout
write_forecasts([]).
write_forecasts([H|T]) :- write_forecast(H),
                          writeln(""),
                          write_forecasts(T).

%! write_forecast(+Forecast) is det.
%  write a single forecast to stdout
write_forecast(FC) :-
    Rain is FC.rain/3,  % FC.rain is for 3 hours
    format("~w @ ~d:00~n", [FC.description, FC.hour]),
    format("~0f°C | ~0fkm/h | ~2fmm | ~d%~n", [FC.temperature
                                              ,FC.wind
                                              ,Rain
                                              ,FC.cloudiness]).
