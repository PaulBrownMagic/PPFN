:- module(date_time, [dict_date_hour/3
                     ,x_days/2
                     ,date_day/2
                     ,day_time_forecasts/2
                     ]
          ).

/*!

Module date_time

Contains utility functors to handle date and time information.

*/

%! date_hour(+DateTime, -Date, -Hour) is det.
%  DateTime: datetime(year, month, day, hour, minute, seconds... etc)
%  Date: date(Year, Month, Day)
%  Hour: integer
%  get the date and hour from a datetime
date_hour(DT, Date, Hour) :-
    date_time_value(date, DT, Date),
    date_time_value(hour, DT, Hour).

%! dict_date_hour(+Dict, Date, Hour) is det.
%  Dict: an incoming forecast dict from open weather api
%  finds the timestamp (dt) in the dict, converts it to
%  a date and hour.
dict_date_hour(Dict, Date, Hour) :-
    Stamp = Dict.dt,
    stamp_date_time(Stamp, DT, local),
    date_hour(DT, Date, Hour), !.

% x_days(+X, -Dates) is det.
% get X number of sequential dates starting from today
x_days(X, Dates):-
    length(Stamps, X),
    from_today_stamps(Stamps),
    stamps_to_dates(Stamps, Dates), !.

%! stamps_to_dates(+Stamps, -Dates) is det.
%  given a list of stamps, convert them all to dates
stamps_to_dates([], []).
stamps_to_dates([S|ST], [D|DT]) :-
    stamp_date_time(S, DateTime, local),
    date_time_value(date, DateTime, D),
    stamps_to_dates(ST, DT).

%! from_today_stamps(-Stamps) is det.
%  generate a bunch of timestamps for sequential days
%  requires a list of vars to limit size
from_today_stamps([Today|Rest]) :-
    get_time(Today),
    timestamps([Today|Rest]), !.

%! timestamps(+Timestamps) is det.
%  make a sequential list of timestamps from an initial timestamp
timestamps([_|[]]).
timestamps([D1, D2|T]) :-
    one_day(Day),
    D2 is D1 + Day,
    timestamps([D2|T]).

%! one_day(-Seconds) is det.
%  get the number of seconds in a day,
%  useful for doing math with timestamp data
one_day(Seconds) :-
    Seconds is 60 * 60 * 24.

%! day(+Num, -Name) is det.
% lookup for day names
day(1, 'Monday').
day(2, 'Tuesday').
day(3, 'Wednesday').
day(4, 'Thursday').
day(5, 'Friday').
day(6, 'Saturday').
day(7, 'Sunday').

%! date_day(+Date, -Day) is det.
%  Find what day of the week the date is.
date_day(Date, Day) :-
    day_of_the_week(Date, Num),
    day(Num, Day).

%! day_time(+Forecast) is det.
%  check if the forecast is during the day, i.e. between 8:00 and 22:00.
day_time(Forecast) :-
    Forecast.hour > 8, Forecast.hour < 22.

%! day_time_forecasts(+Forecasts, -DayTimeForecasts) is det.
%  filter a list of forecasts to include only those that are in the daytime.
day_time_forecasts([], []).
day_time_forecasts([H|T1], [H|T2]) :-
    day_time(H),
    day_time_forecasts(T1, T2).
day_time_forecasts([H|T], Filtered) :-
    \+ day_time(H),
    day_time_forecasts(T, Filtered).


