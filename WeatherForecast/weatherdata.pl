:- module(weatherdata, [get_data/3
                       ,get_daily_data/3
                       ]
         ).

% SWI-Prolog libraries
:- use_module(library(http/json)).
:- use_module(library(http/http_open)).

% Local modules
:- use_module(secret_key, [api_key/1]).
:- use_module(date_time, [dict_date_hour/3
                         ,x_days/2]).
:- use_module(city_ids, [id/3]).

% Cache data
:- dynamic(known/3).

/*
Module weatherdata

Contains functors to import and manipulate weather data
obtained from openweathermap.org.  Requires an openweathermap.org
account with secret key, stored in a module called `secret_key`
as api_key("<KEY>").
*/


%! get_data(City, Country, Data) is det.
%  get simplified weather forecast data
get_data(City, Country, Data) :-
    cached_weather_data(City, Country, D),
    List = D.list,
    describe_all(List, Data).

%! get_daily_data(+City, +Country, -Data) is det.
%  gets simpleified weather forecast and splits it
%  into a 2d list, so each sublist contains a day's
%  worth of forecasts.
get_daily_data(City, Country, Data) :-
    get_data(City, Country, D),
    split_by_day(D, Data).

%! weather_url(+Location, +Country, -URL) is det.
%  create an open weather api url
weather_url(Location, Country, URL) :-
    id(Location, Country, ID),
    api_key(Key),
    format(atom(URL), "https://api.openweathermap.org/data/2.5/forecast?appid=~w&id=~w", [Key, ID]).

%! weather_data(+Location, +Country, -Data) is det.
%  get JSON weather data from open weather api and read in as dict
weather_data(Location, Country, Data) :-
    weather_url(Location, Country, URL),
    setup_call_cleanup(
        http_open(URL, In, [request_header('Accept'='application/json')]),
        json_read_dict(In, Data),
        close(In)
    ).

%! cached_weather_data(+Location, +Country, -Data) is det.
%  retrieve data from cache, or call api and cache it.
%  useful during development so as not to spam the api endpoint
cached_weather_data(Location, Country, Data) :-
    known(Location, Country, Data), ! ;
    weather_data(Location, Country, Data),
    assert(known(Location, Country, Data)).

%! match_record(+Date, +Records, -Match) is nondet.
%  find records that match the given date
match_record(Date, [Match|_], Match) :-
    dict_date_hour(Match, Date, _).
match_record(Date, [_|T], Match) :-
    match_record(Date, T, Match).

%! all_matches(+Date, +Records, -Matches) is det.
%  get all the matches for a date as a list
all_matches(Date, Records, Matches) :- findall(Match, match_record(Date, Records, Match), Matches).

%! split_by_day(+AllDays, -SplitDays) is det.
%  Transform 1D list into 2D by date.
split_by_day(AllDays, SplitDays) :-
    x_days(5, FD),
    split_by_day(AllDays, FD, SplitDays).
split_by_day(_, [], []).
split_by_day(AllDays, [HF|FiveDays], [HS|SplitDays]) :-
    all_matches(HF, AllDays, HS),
    split_by_day(AllDays, FiveDays, SplitDays).

%! describe_all(+APIdicts, -SimpleDicts) is det.
%  simplify the data to only include required information
describe_all([], []).
describe_all([In|IT], [D|DT]) :-
    describe(In, D),
    describe_all(IT, DT).

%! describe(+Record, -Dict) is det.
%  simplify a single record of api data
describe(Record, Dict) :-
    dict_date_hour(Record, Date, Hour),
    cloudiness(Record, Cloudiness),
    temperature(Record, Temp),
    humidity(Record, Humidity),
    rain(Record, Rain),
    wind(Record, Wind),
    description(Record, Desc),
    dt(Record, DT),
    Dict = record{cloudiness:Cloudiness,
                  temperature:Temp,
                  humidity:Humidity,
                  rain:Rain,
                  wind:Wind,
                  description:Desc,
                  dt:DT,
                  hour:Hour,
                  date:Date}.

%! cloudiness(+Record, -Result) is det.
%  extract cloud cover %
cloudiness(Record, Result) :- Result = Record.get(clouds).get(all).

%! temperature(+Record, -Result) is det.
% extract temperature in celcius
temperature(Record, Result) :- Kelvin = Record.main.temp, Result is Kelvin - 273.

%! humidity(+Record, -Result) is det.
% extract % humidity
humidity(Record, Result) :- Result = Record.main.humidity.

%! rain(+Record, -Result) is det.
% extract 3 hour rainfall
rain(Record, Result) :- Result = Record.get(rain).get('3h'), ! ; Result = 0.0.

%! wind(+Record, -Result) is det.
%  extract wind speed in km/h
wind(Record, Result) :- Result = Record.get(wind).get(speed).

%! description(+Record, -Result) is det.
%  extract short weather description
description(Record, Result) :-
    [Weather] = Record.weather,
    Result = Weather.description.

%! dt(+Record, -Result) is det.
%  extract datetime stamp
dt(Record, Result) :-
    Result = Record.get(dt).
