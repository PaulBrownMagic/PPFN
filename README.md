# Prolog Projects For Newbies.

A collection of short projects suitable for beginners in Prolog to practise
their skills and test their learning. Plus they're fun to make and introduce
some good old fashioned artificial intelligence.

## Status

Completed Projects:
- Password Generator: create random passwords
- Space Age: find out how old you'd be on another planet
- WeatherForecast: get a weather forecast, what should you wear?
- Rock Paper Scissors
- Ye Pirate Insults: DCG to generate insults like a pirate, yarr!
- Grubs Up: What could you make with the ingredients in your cupboard?

Ideas (on branches):
- GuessPet: think of a pet, I'll guess it, or I'll think of it, you
  guess it.

Ideas (not on branches):
- ISS station tracker with country information
- DinoFacts random fact about a given dinosaur (DCG?)
- Escape from Zurg ([Original
  Paper](http://web.engr.oregonstate.edu/~erwig/papers/Zurg_JFP04.pdf),
[CLP + DCG Solution](https://www.metalevel.at/zurg/), spin a new
solution with the aim of being easier to read)
