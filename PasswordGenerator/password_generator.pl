:- module(password_generator, [password/2]).

:- use_module(pwgenrandom, [crandom/2
                           ,crandom_member/2
                           ]
             ).


%! letters(Letters) is det.
%  Contains all the letters in the alphabet, lowercase atoms
letters([a, b, c, d, e, f, g, h, i, j, k, l, m,
         n, o, p, q, r, s, t, u, v, w, x, y, z]).

%! punctuation(Punctuation) is det.
%  Chosen punctuation symbols for possible inclusion
punctuation(['#', '$', '&', '(', ')', '|', '£', '?', '!']).

%! random_letter(-L) is det.
% Generates a random character as an atom
random_letter(L) :-
    letters(Letters),
    crandom_member(L, Letters).

%! random_uppercase_letter(-U) is det.
%  Generates a random uppercase letter
random_uppercase_letter(U) :-
    random_letter(L),
    upcase_atom(L, U).

%! random_punctuation(-P) is det.
%  Generates a random member of punctuation
random_punctuation(P) :-
    punctuation(Punctuation),
    crandom_member(P, Punctuation).

%! random_number(-N) is det.
%  Generates a number between 0-9 inclusive
random_number(N) :- crandom(9, N).

%! random_character(-C) is det.
%  Generates a random letter (either case), number or punctuation
random_character(C) :-
    Choices = [random_letter
              , random_uppercase_letter
              , random_punctuation
              , random_number
              ],
    crandom_member(Choice, Choices),
    Random =.. [Choice, C],
    call(Random).

%! password_generate(-P, +L) is det.
%  P: A list of random characters of length L
%  L: An integer number to set the length of the password
password_generate(P, L) :-
    length(P, L),
    make_password(P).

%! make_password(-P) is det.
%  Recursively populate the password list with random characters
make_password([]).
make_password([C|T]) :-
    random_character(C),
    make_password(T).

%! password(-P, +L) is det.
%  Generate a password of length L as a string.
%  P: password, should not be ground when called!
%  L: An integer number to set the length of the password
password(P, L) :-
    password_generate(Password, L),
    atomics_to_string(Password, P).
