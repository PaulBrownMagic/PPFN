:- module(pwgen, [crandom/2
                   ,crandom_member/2
                   ]
         ).

:- use_module(library(clpfd)).
:- use_module(library(crypto)).

/*
Module crandom used as a more cryptographically secure
random generator in place of the SWI-Prolog builtins
random/0 and random_member/2.
*/

%! bytes_integer(+Bytes, -Integer) is det.
bytes_integer(Bs, N) :-
        foldl(pow, Bs, 0-0, N-_).

%! pow(Byte, N0-I0, N-I) is det.
pow(B, N0-I0, N-I) :-
        B in 0..255,
        N #= N0 + B*256^I0,
        I #= I0 + 1.

%! rand(-I) is det.
%  rand creates a 32 bit crypto-standard random number
rand(I) :-
    crypto_n_random_bytes(32, Bs),
    bytes_integer(Bs, I).

%! crandom(+Max, -I) is det.
%  gets a random number, capped to remove modulus bias
crandom(Max, I) :-
    random_int_ceil(Max, C),
    rand(N),
    N < C,
    Maxx is Max + 1,
    I is mod(N, Maxx).

%! crandom_member(-Member, +List) is det.
%  uses crypto random numbers to recreate builtin
crandom_member(Member, List) :-
    length(List, L),
    Decr is L - 1,
    crandom(Decr, I),
    crandom_member(List, 0, I, Member).
crandom_member([Member|_], I, I, Member) :- !.
crandom_member([_|T], Acc, I, Member) :-
    Incr is Acc + 1,
    crandom_member(T, Incr, I, Member).


%! random_int_ceil(-Int) is det.
%  largest allowed random number, it's 5 less than the maximum with 32 bits.
%  This is used to cap the random number to allow for clean modulus of random number,
%  prevents 0, 1, 2, 3, 4, 5 from appearing that tiny, tiny bit more often.
random_int_ceil(Cap, I) :-
    Max = 115792089237316195423570985008687907853269984665640564039457584007913129639935,
    Diff is mod(Max, Cap),
    I is Max - Diff.
