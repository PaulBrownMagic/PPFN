# Space Age

**Also available online via
[SWISH](https://swish.swi-prolog.org/p/SpaceAge.swinb).**

How old are you in Mercury years? How about Neptune years? Let's
find out with a fun little program. First the data, this is
how many planet years there are to one Earth year.

```prolog
%! year_ratio(+Planet, -Ratio) is det.
%  year ratio is the difference between
%  years on Earth and the Planet
year_ratio(mercury, 4.15).
year_ratio(venus, 1.62).
year_ratio(mars, 0.53).
year_ratio(jupiter, 0.08).
year_ratio(saturn, 0.03).
year_ratio(uranus, 0.01).
year_ratio(neptune, 0.006).
```

## Learning

The other bit of information we need is the user's age. Let's ask
them what it is, and then remember it for later. This way when they
use the program, it will only ask them once. This is a good way to
get small bits of information in a small program. For larger use
cases, `assert/1` is a little inefficient.

```prolog
%! age(-Age) is det.
%  if we already know the user's age,
%  use that. Otherwise ask for it.
age(Age) :-
    known(age, Age), ! ;
    ask(age, Age).

%! ask(+Predicate, -Val) is det.
%  ask a user what the value is then
%  store their answer in memory.
ask(Predicate, Val) :-
    write("What is your "),
    write(Predicate),
    writeln("?"),
    read(Val),
    assert(known(Predicate, Val)).
```

We can try this out, it remembers!

```prolog
?- age(Age).
What is your age?
|: 14.

Age = 14.

?- age(Age).
Age = 14.
```

## How old?!

Finally we need a predicate to calculate our spaceage. This looks
up the `year_ratio` for the planet, looks for the user's age (and
asks for it if needed), then calculates the PlanetAge.

```prolog
%! spaceage(+Planet, -PlanetAge) is det.
%  convert the user's age into Planet years.
spaceage(Planet, PlanetAge) :-
    year_ratio(Planet, Ratio),
    age(EarthAge),
    PlanetAge is EarthAge * Ratio.
```

Try it out!

```prolog
?- spaceage(mars, SpaceAge).
```
