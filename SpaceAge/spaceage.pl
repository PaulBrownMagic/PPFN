:- dynamic(known/2).

%! year_ratio(+Planet, -Ratio) is det.
%  year ratio is the difference between
%  years on Earth and the Planet
year_ratio(mercury, 4.15).
year_ratio(venus, 1.62).
year_ratio(mars, 0.53).
year_ratio(jupiter, 0.08).
year_ratio(saturn, 0.03).
year_ratio(uranus, 0.01).
year_ratio(neptune, 0.006).

%! age(-Age) is det.
%  if we already know the user's age,
%  use that. Otherwise ask for it.
age(Age) :-
    known(age, Age), ! ;
    ask(age, Age).

%! ask(+Predicate, -Val) is det.
%  ask a user what the value is then
%  store their answer in memory.
ask(Predicate, Val) :-
    write("What is your "),
    write(Predicate),
    writeln("?"),
    read(Val),
    assert(known(Predicate, Val)).

%! spaceage(+Planet, -PlanetAge) is det.
%  convert the user's age into Planet years.
spaceage(Planet, PlanetAge) :-
    year_ratio(Planet, Ratio),
    age(EarthAge),
    PlanetAge is EarthAge * Ratio.
