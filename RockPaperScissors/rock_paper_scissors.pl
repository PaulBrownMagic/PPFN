/*
*  Rock Paper Scissors!
*
*  Single game:
*  ?- turn.
*
*  Full game:
*  ?- game.
*
*/


%! rule(+Item, -Verb, +Item2) is det.
%  Rules of rock, paper, scissors.
rule(rock, blunts, scissors).
rule(scissors, cut, paper).
rule(paper, wraps, rock).
% Case where players draw by shooting same item
rule(Item, draws, Item).

%! game is det.
%  A game consists of a turn and a query to play again.
game :-
    turn,
    play_again.

%! play_again is det.
%  A query to play again, returns to game if it doesn't read n.
play_again :-
    writeln("Play again?"),
    read(n), ! ;
    game.

%! turn is det.
%  A turn consists of a countdown, player and computer both shoot,
%  the shots are compared and the winner is announced.
turn :-
    countdown(3),
    player_shoot(Item1),
    computer_shoot(Item2),
    compare_shoot(Item1, Item2, Rule),
    congratulate(Item1, Item2, Rule).

%! countdown(+N) is det.
%  countdown from N to shoot, write to stdout.
countdown(0) :-
    writeln("Shoot!").
countdown(N) :-
    format("~d. ", N),
    M is N - 1,
    countdown(M).

%! player_shoot(-Item) is nondet.
%  read the players choice of item, if it's not recognisable ask again
%  until we get rock, paper, or scissors.
player_shoot(Item) :- read(Item), member(Item, [rock, paper, scissors]).
player_shoot(Item) :- player_shoot(Item).

%! computer_shoot(-Item) is det.
%  get a random item for the computer.
computer_shoot(Item) :-
    random_member(Item, [rock, paper, scissors]).

%! compare_shoot(+Item1, +Item2, -Rule) is det.
%  use the rules (`rule/3`) to find which one to apply
compare_shoot(Item1, Item2, Rule) :-
    Rule = rule(Item1, _, Item2), Rule.
compare_shoot(Item1, Item2, Rule) :-
    Rule = rule(Item2, _, Item1), Rule.


%! congratulate(+Item1, +Item2, +Rule) is det.
%  Write out the results of the turn for the player.
congratulate(Item, Item, rule(Item, draws, Item)):-
    format("You both shoot ~w, it's a draw.~n", Item), !.
congratulate(Item1, Item2, rule(Item1, Verb, Item2)):-
    format("~w ~w ~w.~n", [Item1, Verb, Item2]),
    writeln("You Win!"), !.
congratulate(Item1, Item2, rule(Item2, Verb, Item1)):-
    format("~w ~w ~w.~n", [Item2, Verb, Item1]),
    writeln("You Loose"), !.
