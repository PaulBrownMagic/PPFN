%! ingredient(Name, Tags) is semidet.
%  @arg Name A unique identifier for the ingredient
%  @arg Tags A list of terms the user may use when searching
ingredient('Lamb Neck Chops', [lamb]).
ingredient('Medium Brown Onions', [onion, onions]).
ingredient('Large Potatoes', [potato, potatoes]).
ingredient('Bacon Rashers', [bacon]).
ingredient('Beef Stock', []).
ingredient('Butter', []).
ingredient('Beef Chuck Steak', [beef]).
ingredient('Baby Onions', [onion, onions]).
ingredient('Button Mushrooms', [mushrooms, mushroom]).
ingredient('Garlic Clove', [garlic]).
ingredient('Plain Flour', []).
ingredient('Red Wine', []).
ingredient('Bay Leaves', [bay]).
ingredient('Brown Sugar', [sugar]).
ingredient('Chicken Pieces', [chicken]).
ingredient('Spring Onion', [onion, onions]).
ingredient('Chestnut Mushrooms', [mushrooms, mushroom]).
ingredient('Brandy', []).
ingredient('Chicken Stock', []).
ingredient('Fresh Parsley', [parsley]).
ingredient('Thyme', []).
ingredient('Bay Leaf', [bay]).
ingredient('Tomato Paste', ['tomato puree']).
