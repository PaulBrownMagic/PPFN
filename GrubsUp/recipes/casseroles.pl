%! recipe(Name, Ingredients) is semidet.
%  @arg Name A unique identifier for the recipe
%  @arg Ingredients A list of ingredients in `Name-(Quantity, Measurement)` format
recipe('Lancashire Hot Pot',
    [ 'Lamb Neck Chops'-(1, kg)
    , 'Medium Brown Onions'-(450, g)
    , 'Large Potatoes'-(3, count)
    , 'Bacon Rashers'-(4, count)
    , 'Beef Stock'-(430, ml)
    , 'Butter'-(30, g)
    ]
).

recipe('Beef Bourguignon',
    [ 'Beef Chuck Steak'-(1, kg)
    , 'Butter'-(80, g)
    , 'Baby Onions'-(10, count)
    , 'Button Mushrooms'-(400, g)
    , 'Bacon Rashers'-(3, count)
    , 'Garlic Clove'-(1, count)
    , 'Plain Flour'-(25, g)
    , 'Beef Stock'-(250, ml)
    , 'Red Wine'-(250, ml)
    , 'Bay Leaves'-(2, count)
    , 'Brown Sugar'-(1, tbsp)
    ]
).

recipe('Coq Au Vin',
    [ 'Chicken Pieces'-(1.5, kg)
    , 'Plain Flour'-(40, g)
    , 'Butter'-(40, g)
    , 'Garlic Cloves'-(2, count)
    , 'Bacon Rashers'-(3, count)
    , 'Spring Onion'-(10, count)
    , 'Chestnut Mushrooms'-(200, g)
    , 'Brandy'-(2, tbsp)
    , 'Red Wine'-(250, ml)
    , 'Chicken Stock'-(250, ml)
    , 'Fresh Parsley'-(1, tbsp)
    , 'Thyme'-(2, tsp)
    , 'Bay Leaf'-(1, count)
    , 'Tomato Paste'-(2, tbsp)
    ]
).
