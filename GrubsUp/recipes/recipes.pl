:- module(recipes,
    [ get_recipe/2
    , list_recipes/1
    , recipes_containing/2
    ]
).

:- include(casseroles).
:- include(ingredients).


%! get_recipe(?Recipe, ?Ingredients) is semidet.
%  get a recipe! Proxy for encapsulation
%  @arg Recipe The name of the recipe
%  @arg Ingredients A list of Ingredients
get_recipe(Recipe, Ingredients) :-
    recipe(Recipe, Ingredients).


%! list_recipes(-Recipes) is det.
%  Return a list of recipe names
list_recipes(Recipes) :-
    findall(N, get_recipe(N, _), Recipes).


%! recipes_containing(+Requirements, -Recipes) is det.
%  Return a list of the recipes that meet the requirements.
%  @arg Requirements A list of ingredients
%  @arg Recipes A list of Recipe Names
recipes_containing(Requirements, Recipes) :-
    findall(R, (get_recipe(R, I), contains(Requirements, I)), Recipes).

%! contains(+Requirements, +Ingredients) is nondet.
%  Check that the requirements are contained in the
%  recipe's ingredients, using tags if necessary
contains([], _).
contains([R|T], I) :-
    member(R-(_,_),I),
    contains(T, I).
contains([R|T], I) :-
    ingredient(Ing, Tags),
    member(R, Tags),
    member(Ing-(_,_), I),
    contains(T, I).
