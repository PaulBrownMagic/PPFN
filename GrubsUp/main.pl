:- module(main,
    [ print_recipe/1
    , list_recipes/0
    , recipes_containing/1
    ]
).

:- use_module(recipes/recipes,
    [ get_recipe/2
    , list_recipes/1
    , recipes_containing/2
    ]
).

%! print_recipe(?Recipe) is semidet.
%  Print a recipe to stdout in a nice format
%  @arg Recipe The name of a recipe
print_recipe(Recipe) :-
    get_recipe(Recipe, Ingredients),
    format("~n-------------------------~n~w~n-------------------------~n~n", Recipe),
    print_ingredients(Ingredients).

%! print_ingredients(+Ingredients) is det.
%  Print the ingredients part of a recipe to stdout
%  @arg Ingredients A list of ingredients in `Name-(Quantity, Measure)` format
print_ingredients([]) :- format("~n~n"), !.
print_ingredients([I-(N, count)|T]) :-
    format("~w ~w~n", [N, I]), !,
    print_ingredients(T).
print_ingredients([I-(N, M)|T]) :-
    format("~w~w ~w~n", [N, M, I]), !,
    print_ingredients(T).

%! recipes_containing(+Requirements:List) is det.
%  Print a list of all recipes that meet the requirements to stdout
%  @arg Requirements A list of ingredients
recipes_containing(Requirements) :-
    recipes_containing(Requirements, Recipes),
    format("~nRecipes:~n"),
    print_list(Recipes).

%! list_recipes is det.
%  Print a list of recipe names to stdout.
list_recipes :-
    list_recipes(Names),
    format("~nRecipes:~n"),
    print_list(Names).

%! print_list(+List) is det.
%  Print a list to stdout
print_list([]) :-
    format("~n~n"), !.
print_list([H|T]) :-
    format("  * ~w~n", H),
    print_list(T).
