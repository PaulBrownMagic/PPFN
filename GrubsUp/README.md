# Grubs Up!

Here's a clever little database kind of a program that's all about food.
Have you ever looked through your cupboard and wondered what you can
cook with the lonely potato, onion and tin of beans you find there?
Maybe Prolog can help. In this program we have some recipes and
ingredients in our recipe database and an access layer that can get
recipes for us. We also have some fancy printing predicates for a nice
UI.

## Recipes

We begin with `recipes/casseroles.pl`, which is just a database. It contains
a `recipe/2` predicate that has the name of the recipe and a list of
ingredients. Ingredients are formatted `Name-(Quantity, Measure)`, this
is just a choice on my part to squeeze a lot of information into a tiny
representation. It is a `Name-Quantity` pair, but we also need the
`Measure` so that's combined with the `Quantity` in a tuple. If you
wanted more files of dishes, you'll need to make `recipe/2` multifile:

```prolog
:- multifile recipe/2
```

Then we go to `recipes/ingredients.pl`, this database only exists
because I expect to be lazy and not which to type out the proper names
for ingredients. There's clever ways around this but for this little
project I went the easy route and gave them tags. This let's me search
for `onion` and get all different kinds of onion back.

The meat of the project is in `recipes/recipes.pl`. The first predicate
declared is `get_recipe/2`, which practically does nothing but it is very
important. If later on I decide to use someone elses API to get recipe data, I
may find myself with RDF or JSON descriptions of recipes. Or I could increase
the arity of `recipe/2` to include cooking steps or meta data. In this case I
can't use `recipe/2` to access them or their ingredients. So I would have to
change `main.pl`, but with `get_recipe/2`, I only change this one predicate and
we're good to go.

`list_recipes/1` is a utility, it's shorthand so we don't have to
keep writing that `findall/3` predicate. Handy.

The big one is in `recipes_containing/2`. This predicate is using a
`findall/3` again, but in the middle of it we've got that
`contains/2` predicate to make sure what is found meets our
requirements.

`contains/2` is a recursive algorithm designed only to succeed or fail.
So if there's no requirements, it doesn't matter what the
ingredients are, it'll succeed as per the first line.
Then we check to see if the first requirement is a member of the
ingredients, with a bit of extra syntax to match the ingredient
format: `Ingredient-(Quantity, Measure)`, if this succeeds we'll check
the rest of the requirements. Final one is for the case where you write
`onions` instead of `'Medium Brown Onions'`. We get the ingredient
tags and see if `onion` is a member of them, if it is we see if that
ingredient is in the Ingredients. If that succeeds, we check the rest.

## Main.pl

Main is about providing a nice interface, it knows nothing about the
data, only the three predicates it imports from `recipes/recipes.pl`.
All it's doing is adding some printing predicates for stdout, so rather
than reading the data structures returned by `recipe/recipes.pl`, you
can read something a little nicer. This is a decent architecture design
for your program. Although they're just printing predicates,
they're worth a look over if you're in the early stages of
playing with recursion. They contain some case handling, but no
tail call optimisation.

## Example use

Load into SWI-Prolog from bash:

```bash
:~GrubsUp$ swipl main.pl
```

Querying:

```prolog
?- list_recipes.

...

?- print_recipe('Lancashire Hot Pot').

...

?- print_recipe(Recipe).

...

?- recipes_containing(['Butter', 'Beef Stock']).

...

?- recipes_containing([onion, mushrooms]).

```

## What next?

There's so much potential here and I've only scratched the surface. If you're
into Knowledge Representation then this a great project to get your teeth into.
[BBC have a recipe ontology](https://www.bbc.co.uk/ontologies/fo) that might
serve you as a starting point. Plus there's lots of information missing in the
recipes, like how to cook them! You'll need something with steps and ordering,
maybe a list?

At a simpler level, the manner in which ingredients are searched via
tags leaves much to be desired, what if I search for `butter` instead of
`Butter`? Have a play to make it case-insensitive, then automatically
look for plurals.

Want to add a new feature? How about a shopping list predicate where I
tell you what recipes I want to cook and you tell me what to buy? You
might need to convert between measurements for this. Bonus points if I
can tell you which ingredients I have and it skips them. Bonus, bonus
points if I can say I have 500g chicken, the recipe needs 750g chicken,
and it tells me to buy 250g chicken.

Want to play with ingesting data? There are public API's for recipes out
there, sadly many are paid for, but they're out there!
