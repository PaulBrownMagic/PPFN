# Contributions are welcome!

Contributions need not be complete, this is a team effort. So if you have a
great idea for a project, you can just raise an issue. If you've not got an
idea for a project, you can help with coding, copy, images, video, audio,
publishing etc.

## What PPFN is and isn't

PPFN *is* a collection of short projects that are:

- suitable for a teenager and older to tackle
- fun
- short:
  - max 2 sides A4 copy
  - max 500 lines Prolog
  - can be completed in a couple of hours
- freely shared to benefit learners

PPFN *can* include projects that:

- require hardware tinkering with Raspberry Pi (or similar)
- get data from a public source
- are divided into smaller projects
- apply a similar method as another project to a different adventure

PPFN *is not*:

- a Prolog course
- a Prolog text-book
- a Prolog curriculum
- a place to debate Prolog versions, design, language features etc.

When it comes to code style and Prolog version, I, Paul Brown, reserve BDFL
status. I do this because I do not want efforts to detract from the aim of
the project.

## Contribution Process

We'll follow standard git contribution proceedure:

- New project ideas or improvements can be noted as an issue
- Fork, branch, make your changes, commit, submit a pull request for
contributions
- Contributors will be celebrated in CONTRIBUTERS.md

## Which Prolog?

All projects should be able to run in the latest stable version of SWI-Prolog.
SWI-Prolog is being used because it can interface with C, and thus Python
(pyswip), which is the most popular programming language taught in schools. It
also has a decent installation experience and manager with swivm.

Projects that require a GUI may also use Tau Prolog, which will allow projects
that run in a web browser.