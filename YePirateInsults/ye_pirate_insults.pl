:- module(ye_pirate_insults, [insult_me/0]).

/** <module> Ye Pirate Insulter

Use a DCG to generate random pirate insults!

**Example use**:

```
?- insult_me.
Yarr, ye keelhauling, whale-breathing Duffle!
true.

?- insult_me.
Yo Ho, You face-stabbed, broadsiding Scallywag!
true.

?- insult_me.
Walk the plank, ye rope's-ending, chain-dragging Duffle!
true.

?- insult_me.
Blimey! Ye flogging, rum-running Abaft!
true.

?- insult_me.
Walk the plank, ye deck-swabbing, blitherin Freebooter!
true.
```
*/

%! insult_me is det.
%  Generate an insult and use [[format/2]] to print it to the screen
insult_me :-
    insult(Insult, []), format('~s~n', [Insult]).

%! insult is det.
%  An insult consists of an opener,
%  two adjectives for extra emphasis, and a noun
insult --> opener, " ",
           adjective, ", ",
           adjective, " ",
           noun, "!".

%! opener is det.
%  An opener is one of the openers chosen at random
opener --> {random_opener(Opener)}, Opener.

%! adjective is det.
%  An adjective is one of the adjectives chosen at random
adjective --> {random_adjective(Adjective)}, Adjective.

%! noun is det.
%  An noun is one of the nouns chosen at random
noun --> {random_noun(Noun)}, Noun.

%! random_opener(-Opener) is nondet.
%  choose a random opener from openers.
random_opener(Opener) :-
    openers(Openers),
    random_member(Opener, Openers).

%! random_adjective(-Adjective) is nondet.
%  choose a random adjective from adjectives.
random_adjective(Adjective) :-
    adjectives(Adjectives),
    random_member(Adjective, Adjectives).

%! random_noun(-Noun) is nondet.
%  choose a random noun from nouns.
random_noun(Noun) :-
    nouns(Nouns),
    random_member(Noun, Nouns).

%! openers(-Openers:list) is det.
%  A list of phrases suitable for opening
%  a piratey insult.
openers([ "Ye"
        , "Ahoy! Ye"
        , "Walk the plank, ye"
        , "Yo Ho, You"
        , "Yarr, ye"
        , "Shiver my timbers, ye"
        , "Blimey! Ye"
        , "Listen, bucko, you"
        ]
    ).

%! adjectives(-Adjectives:list) is det.
%  Phrases with a piratey flavour to describe
%  the unfortunate victim of the insult.
adjectives([ "scurvy"
           , "Hornswaggling"
           , "hog-sweating"
           , "Coaming"
           , "mud-spewing"
           , "hook-lover"
           , "blitherin"
           , "Davy Jonesing"
           , "fish-feeding"
           , "scuttling"
           , "shark-punching"
           , "spewing"
           , "piratey"
           , "3 sheets to the wind"
           , "bedraggled"
           , "narwhal-smelling"
           , "plait bearded"
           , "poop deck"
           , "half-breathed"
           , "face-stabbed"
           , "duck-knee'd"
           , "scar-faced"
           , "thumb-suckin'"
           , "dung munchin"
           , "pig-faced"
           , "jelly-boned"
           , "lard-brained"
           , "baboon-smelling"
           , "snivelin'"
           , "slimy"
           , "hornswaggling"
           , "wiggly"
           , "daft"
           , "broadsiding"
           , "bilge-drinking"
           , "rum-running"
           , "chimp-faced"
           , "conch-smelling"
           , "fathom-head"
           , "killick-brained"
           , "fork-faced"
           , "rope's-ending"
           , "bilge-sucking"
           , "flogging"
           , "keelhauling"
           , "grog-drinking"
           , "loot-stealing"
           , "marooning"
           , "squiffy"
           , "plundering"
           , "cutlass-swinging"
           , "mutiny-starting"
           , "eel-smelling"
           , "deck-swabbing"
           , "whale-breathing"
           , "brisket-eating"
           , "rat-eating"
           , "ship-sinking"
           , "swashbucklin'"
           , "timber-rattling"
           , "kracken-smelling"
           , "bilge-swimming"
           , "pustulant"
           , "mangy"
           , "chain-dragging"
           , "pox-ridden"
           ]).

%! nouns(-Nouns:list) is det.
%  What the unfortunate victim will
%  be described as in our piratey insult.
nouns([ "hearties"
      , "black spot"
      , "dungbie"
      , "Hempen halter"
      , "Abaft"
      , "barnacle"
      , "Cackle fruit"
      , "Duffle"
      , "Holystone"
      , "Monkey jacket"
      , "Orlop"
      , "Coxswain"
      , "Flibustier"
      , "bilge rat"
      , "Landlubber"
      , "Freebooter"
      , "Powder monkey"
      , "matey"
      , "gibbet"
      , "Jolly Roger"
      , "man-o-war"
      , "old salt"
      , "Scallywag"
      , "seadog"
      , "shark-bait"
      , "coghead"
      , "cockroach"
      , "son of a sea cook"
      , "crud bucket"
      , "bat-spit"
      , "maggot"
      , "snot-rag"
      , "squid"
      ]).
