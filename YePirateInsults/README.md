# A Pirate Insult Generator

Prolog has a long history of use in language processing, I'm sure
Pirate insults were just what the genius folk who developed it had
in mind!

We'll start off with some data.

## Bits of Insults

Our insult needs a good opener, some adjectives to describe our victim,
and a noun to call our victim. So we'll start with that. This data
comes from
[PlayingWithPlays](https://playingwithplays.com/wp-content/uploads/2014/09/PIrate-Insult-Generator.pdf),
it's shown here in brief, but included in Prolog format in the file.

```prolog
openers([ "Ye"
        , "Ahoy! Ye"
        , "Walk the plank, ye"
        ]
    ).

adjectives([ "scurvy"
           , "Hornswaggling"
           , "hog-sweating"
           , "Coaming"
           ]
    ).

nouns([ "hearties"
      , "black spot"
      , "dungbie"
      , "Hempen halter"
      ]
    ).
```

## Choosing Bits of Insults

We've got our lists of words to use, but we need a way to choose random
ones. We'll use the builtin predicate `random_member/2` to choose from
our lists.

```prolog
%! random_opener(-Opener) is nondet.
%  choose a random opener from openers.
random_opener(Opener) :-
    openers(Openers),
    random_member(Opener, Openers).

%! random_adjective(-Adjective) is nondet.
%  choose a random adjective from adjectives.
random_adjective(Adjective) :-
    adjectives(Adjectives),
    random_member(Adjective, Adjectives).

%! random_noun(-Noun) is nondet.
%  choose a random noun from nouns.
random_noun(Noun) :-
    nouns(Nouns),
    random_member(Noun, Nouns).
```

## Making the Insult
Now we need to stick it all together, this is where DCG's are very
useful. They're an efficient way of processing grammers and language.

We'll start with our insult, to make it extra insulting, we'll use two
adjectives. We also add in some spaces so it's nice to read. This rule
reads as "An insult is an opener, a space, an adjective, a comma and
space, another adjective, a space, a noun and an exclamation mark."

```prolog
%! insult is det.
%  An insult consists of an opener,
%  two adjectives for extra emphasis, and a noun
insult --> opener, " ",
           adjective, ", ",
           adjective, " ",
           noun, "!".
```

Now we need to tell Prolog what an opener, adjective and noun are. We've
got our predicates for getting them at random, so we put these into
`{}`. This will make Prolog run the predicate inside the `{}` as a goal,
instead of trying to include it as part of the grammer. We need it to
run before it unifies `opener` with `Opener`, so we put it first.

```prolog
%! opener is det.
%  An opener is one of the openers chosen at random
opener --> {random_opener(Opener)}, Opener.

%! adjective is det.
%  An adjective is one of the adjectives chosen at random
adjective --> {random_adjective(Adjective)}, Adjective.

%! noun is det.
%  An noun is one of the nouns chosen at random
noun --> {random_noun(Noun)}, Noun.
```

This will now generate our insults.

## Making it Readable
The insults are generated looking like lists of numbers, because of how
Prolog understands strings written in "double-quotes". So our last
predicate will generate an insult and format it to the screen so we can
read it. If you're wondering why `insult(Insult, [])` has the empty
list, I'd recommend reading about DCGs in Prolog.

```prolog
%! insult_me is det.
%  Generate an insult and use [[format/2]] to print it to the screen
insult_me :-
    insult(Insult, []), format('~s~n', [Insult]).
```

## What Next?
Shakespearian insult generators are also popular, you can get the
required data from [The Theatre Folk](https://www.theatrefolk.com/freebies/shakespearean-insults.pdf), the third column is the nouns.

Why are they always insult generators?! How about making a nice
complement generator? That'll cheer you up in the morning.
